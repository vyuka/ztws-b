# 2. tematická práce

## Obsah
1. [Zadání](#zadání)
2. [Technické detaily](#technické-detaily)
3. [Podmínky splnění](#podmínky-splnění)

## Zadání

Cílem této práce je vytvořit webové rozhraní pro vyhledávání v informačním systému [STAG](http://stag.ujep.cz/).
Podobně jako v první tematické práci bude aplikace využívat PHP knihovny pro tvorbu HTTP požadavků [Guzzle](https://github.com/guzzle/guzzle).

Narozdíl od předchozí práce, kde primární rozhraní pro interakci s aplikací byla systémová konzole, bude v této aplikaci PHP využito
společně s jazyky HTML a CSS a jejich grafickými možnostmi.

Součástí zadání jsou i čtyři HTML soubory obsahující nakódovanou stránku. Obsahy těchto souborů využijte při implementaci jednotlivých stránek.
Aplikace bude mít následující složkovou a souborovou strukturu:

```
projekt
├── partials
│   ├── footer.php
│   └── header.php
├── public
│   └── index.php
├── src
│   └── Nav.php
├── vendor
│   └── autoload.php
└── views
    ├── home.php
    ├── rooms.php
    ├── subjects.php
    └── thesis.php
```

Aplikaci spouštějte ze složky projektu příkazem:

```php
php -S localhost:8000 -t public
```

Složka `public` bude sloužit jako takzvaný kořenový adresář webového serveru.
Požadavky směřující na aplikaci budou směřovány do této složky. Pokud např. aplikace poběží na adrese localhost:8000, požadavek http://localhost:8000/hello.txt bude vyžadovat soubor ve složce `projekt/public/hello.txt`.

Soubor `public/index.php` bude hlavním vstupním bodem do aplikace a bude skládat jednotlivé části stránky.

Ve složce `partials` budou soubory obsahující sdílený HTML obsah, jako je navigace a zápatí - `footer.php`, `header.php`.

Složka `views` ponese unikátní obsah jednotlivých stránek aplikace. Soubory v této složce se budou v souboru `index.php` podmíněně vkládat dle parametru požadavku (query parameter) `p`.

Př.: `index.php?p=thesis -> zobrazí se thesis.php`.

Toto skládání probíhá v souboru `index.php`, který kromě dynamického obsahu vkládá i sdílené části ze souborů `footer.php` a `header.php`.

## Technické detaily

1. `header.php`

    Kromě společných HTML značek jako jsou `style`, `script`, `link`, aj. bude v tomto souboru obsažena navigace stránek. Tato navigace nebude
    v souboru umímstěna staticky, ale bude vytvořena PHP skriptem, konkrétně voláním metody `render()` na instanci třídy `Nav`.

    ```php
    <?php
        $nav = new JmenoPrijmeni\StagWeb\Nav([
            'home' => 'Domů',
            'subjects' => 'Přemdměty',
            'rooms' => 'Místnosti',
            'thesis' => 'Diplomové práce'
        ]);

        $nav->render($_GET);
    ?>
    ```

    Třída `Nav` bude umístěna v souboru `src/Nav.php` ve jmenném prostoru `JmenoPrijmeni/StagWeb`, kde `JmenoPrijmeni` bude nahrazeno za jméno a příjmení studenta.

2. `složka src`

    Tato složka bude obsahovat PHP objekty používané v aplikaci (např. třídu `Nav`). V souboru `composer.json` bude specifikováno `PSR-4` mapování jmenného prostoru `JmenoPrijmeni/StagWeb` do této složky (`src`).

3. `src/Nav.php`

    Jak již bylo naznačeno výše, třída `Nav` obsahující jedinou metodu `render()` obstarává řádné vykreslování navigace do stránky. V konstruktoru přijímá za prametr asociativní pole, které mapuje tzv. `slug` stránky (URL přívětivou podobu názvu) na text, který je zobrazený uživateli.

    Metoda `render` přijímá za parametr super-globální pole `$_GET`, dle kterého vyhodnotí, zda daná položka menu bude obsahovat třídu `active`. Jinými slovy, zda bude třída zvýrazněna.

    V **konstruktoru i v metodě specifikujte datový typ přijímaného parametru** (`array`).

4. `index.php`

    Kromě již zmíněného vykreslování obsahu bude na začátku souboru přidána kontrola, která ověří přítomnost query parametru `p` v adrese požadavku. Pokud daný parametr nebude existovat, bude uživatel přesměrován na adresu `/index.php?p=home`.

5. `subjects.php`

    Na řádku 67 v souboru `subjects.html` je vyzobrazena značka `div`, která představuje informativní okénko, které se zobrazí v případě chyby. Jedním takovým příkladem je, když uživatel odešle formulář bez vyplnění parametrů. Dalším může být např. chyba při HTTP požadavku na službu STAG.

    Počínaje řádkem 99 se v kódu nachází rozbalovací pole (select) pro výběr jedné ze zadaných možností - katedry. Toto pole však nebude staticky, neb se dále využívá i při vyhledávání místností a diplomových prací.

    Namísto toho bude existovat třída v souboru `src/Repository/DepartmentRepository.php`. Tato třída bude mít definovanou jednu metodu `getDepartments`, která bude vracet pole polí obsahující jednotlivé katedry v následující podobě:

    ```php
    <?php
    $departmentRepository = new DepartmentRepository();
    $departments = $departmentRepository->getDepartments();

    var_dump($departments);

    // Vrací něco ve smyslu:
    [
        ['shortcut' => 'KI', 'name' => 'Katedra Informatiky'],
        ['shortcut' => 'KMA', 'name' => 'Katedra Matematiky'],
        ...
    ]
    ```

    V šabloně stránky `subjects.php` bude využita tato třída a její metoda pro získání seznamu kateder a následně pomocí cyklu foreach vykreslena.

    Podobně dynamicky budou vypsány i možnosti select pro výběr roku.
    Bude existovat třída `src/DateTime/Calendar.php`, jejíž metoda `getLastYears($count)` s parametrem udávající počet let vrátí pole obsahující daný počet let počínaje letošním rokem, př.:

    ```php
    <?php
    $calendar = new Calendar.php;
    $years = $calendar->getLastYears(3);
    // vrátí něco podobného:
    [
        2020,
        2019,
        2018
    ]
    ```

    Obdobně pomocí cyklu foreach budou dané roky vypsány do HTML.

    Pod formulářem se nachází tabulka s výpisem dat, do které budou vykresleny informace získané z API STAG. Pokud nebudou nalezeny žádné záznamy, místo tabulky se zobrazí box naznačený v souboru `subjects.html` na řádku 147.

6. `rooms.php`

    Pro vyhledávání místností platí totéž, co pro předměty.

7. `thesis.php`

    Pro vyhledávání diplomových prací platí totéž, co pro předměty.

8. `footer.php`

    Ve společném obsahu patičky bude dynamicky vložený rok v části `copyright`.

    Tj. `2017-<dynamicky_vložený_letošní_rok>.
## Podmínky splnění

Tematická práce bude uznána pokud:

1. bude odevzdána nejpozději **4. 1. 2021**
2. bude splňovat uvedené zadání (v případě nejasnosti bude platit dodatečné upřesnění)
3. bude vypracována samostatně

(odevzdávejte zip archiv obsahující zdrojové kódy vaší aplikace pojmenovaný ki0196-stagweb-<os. číslo>-<prijmeni>-<jmeno>.zip, do archivu nepřikládejte složku vendor!)