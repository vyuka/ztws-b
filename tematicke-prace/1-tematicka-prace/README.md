# 1. tematická práce

## Obsah
1. [O práci](#o-práci)
2. [Podmínky splnění](#podmínky-splnění)
3. [Zadání](#zadání)

## O práci

Cílem této úlohy je vytvořit v PHP konzolovou aplikaci, která bude sloužit jako rozhraní pro komunikaci s [veřejnou částí aplikačního rozhraní](https://ws.ujep.cz/ws/web?pp_locale=cs&selectedTyp=REST&pp_reqType=render&pp_page=serviceList) (API) informačního systému STAG.

## Podmínky splnění

1. Práce je odevzdaná a schválená nejpozději do **4. 12. 2020** (odevzdávejte zip archiv obsahující zdrojové kódy vaší aplikace pojmenovaný **ki0196-stagcli-<os. číslo>-<prijmeni>-<jmeno>.zip**, do archivu nepřikládejte složku **vendor**!)
2. Práce po funkční stránce splňuje požadavky zadání (vyhledávání v IS STAG)
3. Práce využívá obě předepsané knihovny (**symfony/console, guzzle/guzzle**)
4. Práce využívá správce balíčků **composer**
5. Vlastní kód práce je strukturován dle [PSR-4](https://www.php-fig.org/psr/psr-4/) a využívá autoloading nástroje **composer** (base namespace pro autoloading zvolte ve formátu <JmenoPrijmeni>\StagCli - př.: DanCharousek\StagCli)
6. Práce je vypracovaná **samostatně s možností vyhledávání informací na internetu
7. Kód je psaný objektově** (využívá OOP mechanismů jazyka PHP)

## Zadání

Užitím knihoven [symfony/console](https://github.com/symfony/console) a [guzzle/guzzle](https://github.com/guzzle/guzzle) vytvořte konzolovou aplikaci s následujícím použitím:

1. php app.php find:subjects
2. php app.php find:rooms
3. php app.php find:thesis

**V první variantě** se aplikace interaktivně dotazuje na parametry vyhledávání, dle kterých uživatel může filtrovat jednotlivé předměty. Aplikace bude podporovat následující kritéria vyhledávání:
nazev - libovolně zadaný řetězec zakódovaný pro použití v URI
pracoviste - výběr z fixního seznamu 5 pracovišť
Zkratka - libovolně zadaný řetězec
rok - číslo udávající rok, ve kterém se dané předměty vyučují

**V druhé variantě** nedochází po spuštění k interakci mezi uživatelem a aplikací. Parametry jsou definovány při spuštění příkazu formou tzv. options a arguments. Př. vyhledávání místnosti dle pracoviště: php app.php find:rooms --department=KI

V této variantě bude možné filtrovat dle těchto options:
--building - vyhledává dle zkratky budovy
--room-number - vyhledává dle čísla místnosti
--department - vyhledává dle pracoviště
--max-people - vyhledává dle maximální kapacity osazenstva
--min-people - vyhledává dle minimální kapacity místnosti

**V poslední, třetí variantě** budou použity oba přístupy pro filtrování diplomových prací studentů. Filtrovat bude možné dle:
--assigned-at - parametr zadaný při spuštění příkazu, filtruje práce dle roku zadání
--defense-at - parametr zadaný při spuštění, příkazu, filtruje práce dle roku obhajoby
osCislo - parametr interaktivně vyžádaný od uživatele po spuštění, filtruje práce dle osobního čísla studenta
katedra - parametr interaktivně vyžádaný od uživatele po spuštění příkazu, filtruje práce dle katedry

Dokumentace webových služeb IS STAG je k dispozici na adrese [https://ws.ujep.cz/ws/web?pp_locale=cs&selectedTyp=REST&pp_reqType=render&pp_page=serviceList](https://ws.ujep.cz/ws/web?pp_locale=cs&selectedTyp=REST&pp_reqType=render&pp_page=serviceList).