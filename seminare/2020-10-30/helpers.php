<?php

namespace DanCharousek\CliHelpers;

function writeln($mask, ...$params) // DanCharousek\CliHelpers\writeln()
{
    echo sprintf($mask, ...$params) . PHP_EOL;
}

class Writer
{
    public static function writeln($mask, ...$params)
    {
        echo sprintf($mask, ...$params) . PHP_EOL;
    }
}

// charousek.dev
// dev.charousek.php.support.writeln

//function DanCharousek/CliHelpers/WriteLn($mask, ...$params)
//{
//    echo sprintf($mask, ...$params) . PHP_EOL;
//}
//
//function DanCharousek/CliHelpers/Write($mask, ...$params)
//{
//    echo sprintf($mask, ...$params);
//}