<?php

class Foo
{
    public const BAZ = 42;

    public static $abc = 'Ipsum';

    public string $bar;
    private int $age;

    public function __construct($age = 20)
    {
        $this->bar = 'Unit';

        $this->age = $age;
    }

    public function printBar()
    {
        echo $this->bar;
    }

    public function setProperty()
    {
        $this->age = 30;
    }
}

interface Fooable
{
    public function foo();
}

interface Barable
{
    public function bar();
}

class CanDoFoo implements Fooable, Barable
{
    public function foo()
    {

    }

    public function bar()
    {
        // TODO: Implement bar() method.
    }
}

class A
{
    public function doSomething()
    {

    }
}

class B extends A
{
    public function doSomethingSimilar()
    {
        parent::doSomething();
    }
}

(new B())->doSomething();
(new B())->doSomethingSimilar();

// Outside

$obj = new Foo();
$obj->bar = 'Hello Universe';
$obj->printBar();

$objB = new Foo();
$objB->bar = 'Hello World!';
$objB->printBar();

Foo::$abc = 'lorem';
echo Foo::$abc;