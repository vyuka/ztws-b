<?php

interface Actionable
{
    public function execute();
}