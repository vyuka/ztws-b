<?php

class App
{
    private array $actions = [];

    private string $logFilePath = '';

    private array $exitKeyWords = [];

    public function addAction(string $actionName, Actionable $action)
    {
        // TODO: Kontrolovat, zda akce s tímto názvem již neexistuje
        $this->actions[$actionName] = $action;
    }

    /**
     * @param string $logFilePath
     */
    public function setLogFilePath(string $logFilePath): void
    {
        $this->logFilePath = $logFilePath;
    }

    /**
     * @param array $exitKeyWords
     */
    public function setExitKeyWords(array $exitKeyWords): void
    {
        $this->exitKeyWords = $exitKeyWords;
    }

    public function run()
    {
        while (true) {
            echo 'Choose an action:';

            $actionName = trim(readline());

            if (in_array($actionName, $this->exitKeyWords)) {
                echo 'Goodbye';
                exit(0);
            }

            if (!array_key_exists($actionName, $this->actions)) {
                echo sprintf('Action %s does not exist', $actionName);
                echo 'Choose something better' . PHP_EOL;
                continue;
            }

            /** @var Actionable $action */
            $action = $this->actions[$actionName];

            $action->execute();
        }
    }
}