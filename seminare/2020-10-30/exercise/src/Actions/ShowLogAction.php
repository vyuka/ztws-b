<?php

namespace DanCharousek\App\Actions;

class ShowLogAction implements Actionable
{
    public function execute()
    {
        echo 'Hello from show action';
    }
}