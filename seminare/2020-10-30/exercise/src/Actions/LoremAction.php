<?php

namespace DanCharousek\App\Actions;

require_once __DIR__ . '/Actionable.php';

class LoremAction implements Actionable
{
    public function execute()
    {
        echo 'Hello from LoremAction' . PHP_EOL;
    }
}