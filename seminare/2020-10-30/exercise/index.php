<?php

require_once __DIR__ . '/src/App.php';
require_once __DIR__ . '/src/LoremAction.php';
require_once __DIR__ . '/src/ShowLogAction.php';

$app = new App();

$app->addAction('lorem', new LoremAction());
$app->addAction('showlog', new ShowLogAction());

$app->setLogFilePath('log.txt');
$app->setExitKeyWords(['q', 'quit', 'exit']);

$app->run();