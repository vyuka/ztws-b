<?php

declare(strict_types=1);

// Řádkové komentáře
#  Řádkové komentáře
/*
    Víceřádkové komentáře
    ...
*/

$fooBazBar = 'Hello world! $bar';

$bar = 'Hello' . ' ' . 'world!';

$name = 'John Doe';
$age = 42;

echo sprintf('My name is %s and my age is %d', $name, $age) . PHP_EOL;
echo "My name is $name and my age is $age";

if (42 === '42') { // strict
    echo 'Ano';
} else {
    echo 'Ne';
}

echo PHP_EOL;

function myFunction(int $age): string
{
    $a = 42;
    return "Hello world! $a";
}

echo myFunction(42);