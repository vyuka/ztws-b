<?php

// 1.
//if (array_key_exists('firstName', $_GET)) {
//    $firstName = $_GET['firstName'];
//} else {
//    $firstName = '';
//}
//
//if (array_key_exists('lastName', $_GET)) {
//    $lastName = $_GET['lastName'];
//} else {
//    $lastName = '';
//}

// 2.
//$firstName = array_key_exists('firstName', $_GET) ? $_GET['firstName'] : '';
//$lastName = array_key_exists('lastName', $_GET) ? $_GET['lastName'] : '';

// Null coalesci... operator
$firstName = $_GET['firstName'] ?? '';
$lastName = $_GET['lastName'] ?? '';

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>

<body>
    <form method="get">
        Jméno: <input name="firstName" type="text"><br>
        Příjmení: <input name="lastName" type="text"><br>
        <button type="submit">Odeslat</button>

        <hr>

        <?php
        if ($firstName !== '') {
            echo sprintf('<b>%s</b>', $firstName);
        }
        ?>

        <?php
        if ($lastName !== '') {
            echo sprintf('<i>%s</i>', $lastName);
        }
        ?>
    </form>
</body>
</html>