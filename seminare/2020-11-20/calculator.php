<?php

$numOne = $_GET['numOne'] ?? null;
$numTwo = $_GET['numTwo'] ?? null;

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>PHP Calculator</title>
</head>
<body>
    <form method="get">
        <input name="numOne" type="number"> + <input name="numTwo" type="number">
        <button type="submit">Sečíst</button>
    </form>

    <?php if ($numOne !== null && $numTwo !== null) {
      echo sprintf('<p>%d + %d je: %d</p>', $numOne, $numTwo, $numOne + $numTwo);
    }
    ?>
</body>
</html>