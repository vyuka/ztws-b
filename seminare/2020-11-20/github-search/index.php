<?php

require_once __DIR__ . '/vendor/autoload.php';

$client = new \GuzzleHttp\Client([
    'base_uri' => 'https://api.github.com'
]);

$response = $client->request('GET', '/users?per_page=10');

$rawBody = (string)$response->getBody();
$data = json_decode($rawBody, true); // [ { "login": "..." }, {}, {} ]

//$users = array_map(function ($user) {
//    return $user['login'];
//}, $data);

$users = array_map(fn($user) => $user['login'], $data);

$selectedUser = $_GET['username'] ?? null;

$followers = [];

if ($selectedUser !== null) {
    $response = $client->get(sprintf(
        '/users/%s/followers',
        $selectedUser
    ));

    $rawBody = $response->getBody()->__toString();
    $data = json_decode($rawBody, true);

    $followers = array_map(
        fn($follower) => $follower['login'],
        $data
    );
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <form method="get">
        <select name="username">
            <option value="">-- Selected an user --</option>
            <?php foreach($users as $user): ?>
                <option><?= $user ?></option>
            <?php endforeach; ?>
        </select>

        <button type="submit">Show followers</button>

        <?php if ($selectedUser !== null): ?>
            <ul>
                <?php foreach($followers as $follower): ?>
                    <li><?= $follower ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </form>
</body>
</html>