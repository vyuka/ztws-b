<?php

if (array_key_exists('login', $_POST)) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if ($username === 'admin' && $password === 'admin') {
        echo 'Jste přihlášený';
    } else {
        echo 'Nejste přihlášený';
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Query params</title>
</head>
<body>
<form method="post">
    Jméno: <input name="username" type="text"><br>
    Heslo: <input name="password" type="password"><br>
    <button type="submit" name="login">Přihlásit</button>
</form>

<hr>

<h2>GET</h2>
<?php var_dump($_GET); ?>

<h2>POST</h2>
<?php var_dump($_POST); ?>
</body>
</html>