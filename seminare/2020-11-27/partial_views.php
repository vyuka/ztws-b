<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Moje stránka</title>

    <style></style>
    <script></script>
</head>
<body>
    <nav>
        <ul>
            <li>
                <a href="partial_views.php?p=home">
                    Home
                </a>
            </li>

            <li>
                <a href="partial_views.php?p=about">
                    About
                </a>
            </li>

            <li>Menu C</li>
            <li>Menu D</li>
        </ul>
    </nav>
    <main>
        <?php
        $p = trim($_GET['p'] ?? 'home', './');
//        echo $p;
        $path = sprintf('./views/%s.php', $p);
//        $path = sprintf('./views/../hesla.php', $p);

        if (file_exists($path)) {
            include $path;
        } else {
            include './views/404.php';
        }
        ?>
    </main>
    <footer>
        Copyright
    </footer>
</body>
</html>