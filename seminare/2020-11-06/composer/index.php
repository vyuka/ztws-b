<?php

use Faker\Factory;

require_once __DIR__ . '/vendor/autoload.php';

$faker = Factory::create();

echo $faker->address . PHP_EOL;
echo $faker->name . PHP_EOL;
echo $faker->text . PHP_EOL;

new \wangcj\helloworld\helloworld();