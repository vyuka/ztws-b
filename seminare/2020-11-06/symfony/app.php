<?php

require_once __DIR__ . '/vendor/autoload.php';

use DanCharousek\SymfonyConsoleApp\Commands\HelloWorldCommand;
use DanCharousek\SymfonyConsoleApp\Commands\HelloWorldCommand2;
use Symfony\Component\Console\Application;

$app = new Application();

$app->add(new HelloWorldCommand());
$app->add(new HelloWorldCommand2());

$app->run();