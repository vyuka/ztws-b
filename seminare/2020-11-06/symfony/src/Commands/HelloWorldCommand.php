<?php

namespace DanCharousek\SymfonyConsoleApp\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class HelloWorldCommand extends Command
{
    public static $defaultName = 'stag:findSubjects'; // php app.php hello

    public function configure()
    {
        $this->setDescription('This command does some stuff...')
            ->addArgument('name', InputArgument::REQUIRED)
            ->addOption('uppercase', 'u', InputOption::VALUE_NONE) // --uppercase -u
            ->addOption('count', 'c', InputOption::VALUE_REQUIRED); // --count -c
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $name = $input->getArgument('name');

        $count = $input->getOption('count');

        $upperCase = $input->getOption('uppercase');

        for ($i = 0; $i < $count; $i++) {
            if ($upperCase) {
                echo 'HELLO';
            } else {
                echo 'hello';
            }

            echo PHP_EOL;
        }

//        $io->title('Hello there ' . $name);
//
//        $io->choice('Choose wisely: ', [
//            'Hello',
//            'World'
//        ]);
//
//        $color = $io->ask('What is your favorite colour?');
//
//        $io->writeln('Your color is ' . $color);
//        $io->askHidden('Enter password: ');
//
//        $io->confirm('Pokračovat?');

        return Command::SUCCESS;
    }
}