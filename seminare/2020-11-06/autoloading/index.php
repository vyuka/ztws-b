<?php

//use DanCharousek\OOP\Bar;

spl_autoload_register(function ($name) {
    // $name = "DanCharousek\OOP\Bar";
    $path = sprintf(
        '%s/%s.php',
        __DIR__,
        str_replace('\\', '/', $name)
    );

//    echo $path . PHP_EOL;

    require_once $path;
});

//require_once __DIR__ . '/Foo.php';

// 1. instance foo
// 2. foo neexistuje
// 3. volání autoload funkcí

$foo = new Foo();

$bar = new DanCharousek\OOP\Bar();
//$baz = new Baz();

echo 'There';