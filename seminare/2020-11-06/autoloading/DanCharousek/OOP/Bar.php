<?php

namespace DanCharousek\OOP;

// FQCN
// DanCharousek\OOP\Bar

class Bar
{
    public function __construct()
    {
        echo 'Bar' . PHP_EOL;
    }
}