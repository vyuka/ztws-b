<?php

require_once __DIR__ . '/vendor/autoload.php';

// A\B\C\D -> "asdf" A\B\C\D\E\F\Foo -> "asdf/E/F/Foo.php"
//spl_autoload_register(function () {
//
//});

$faker = \Faker\Factory::create();
echo $faker->name . PHP_EOL;

$foo = new \DanCharousek\PSR4Demo\Foo();
$fooTest = new \DanCharousek\PSR4DemoTests\FooTest();
$bar = new \DanCharousek\PSR4Demo\Utils\Bar();