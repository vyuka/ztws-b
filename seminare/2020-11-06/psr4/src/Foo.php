<?php

namespace DanCharousek\PSR4Demo;

class Foo
{
    public function __construct()
    {
        echo 'Foo' . PHP_EOL;
    }
}