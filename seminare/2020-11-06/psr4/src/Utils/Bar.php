<?php

namespace DanCharousek\PSR4Demo\Utils;

class Bar
{
    public function __construct()
    {
        echo 'Bar' . PHP_EOL;
    }
}