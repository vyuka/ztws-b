<?php

namespace DanCharousek\GuzzleSymfonyDemo\Commands;

use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class HelloWorldCommand extends Command
{
    public static $defaultName = 'find:subjects';

    protected function configure()
    {
        $this->addOption('department', 'd', InputOption::VALUE_REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $department = $input->getOption('department'); // KI/KMA/KFY | null

//        var_dump($department);
//        echo $input->getArgument('file');

        $client = new Client([
            'base_uri' => 'https://ws.ujep.cz/ws/services/rest2/',
            'headers' => [
                'Accept' => 'application/json'
            ]
        ]);

        $url = 'predmety/najdiPredmety?';

//        if ($department) {
//            $url = $url . 'adasd';
//            $url .= 'pracoviste=' . $department . '&';
//        }

        $query = [];

        if ($department) {
            $query['pracoviste'] = $department;
        }

        $query['nazev'] = urlencode('Úvod do Vue.js');

        // ?a=foo&b=bar&c=baz
        $res = $client->get($url, [
//            'headers' => [
//                'Accept' => 'application/json'
//            ],
            'query' => $query
        ]);

        $status = $res->getStatusCode();

//        if ($status !== 200) {
//            echo 'Něco je špatně';
//            die;
//        }

        $body = $res->getBody()->getContents();
        $data = json_decode($body, true);

        var_dump($data);

//        $io->title('Hello from command!');
//
//        $io->table(
//            ['Název', 'Katedra', 'Rok'],
//            [
//                ['Základy tvorby webových stránek B', 'KI', 2020],
//                ['Základy tvorby webových stránek A', 'KI', 2020],
//            ]
//        );

        return Command::SUCCESS;
    }
}