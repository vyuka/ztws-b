<?php

$numbers = [1, 2, 3, 4, 5];

// echo $numbers[0];
// echo $numbers[1];

$numbers[] = 6;

$filtered = array_filter($numbers, function ($item) {
    return $item !== 1;
});

// $filtered = array_values($filtered);

// unset($numbers[0]);

var_dump($filtered);

// for ($i = 0; $i < count($numbers); $i++) {

// }

foreach ($numbers as $key => $number) {
    echo $key . ": ". $number . PHP_EOL;
}

// var_dump($numbers);

$usersByName = [
    'john' => 'John Doe',
    'bob' => 'Bob Marley',
    // ...
];

$usersByName['bob'];