<?php

// echo $argv[0] . PHP_EOL;
// echo $argv[1] . PHP_EOL;
// echo $argc;

// echo 'Hello World!';

if ($argc < 2) {
    echo 'Parameter max number is required';
    exit(1);
}

$maxNumber = $argv[1];
$guessNumber = rand(0, $maxNumber);

// echo $guessNumber;

while (true) {
    echo 'Guess the number: ';

    $input = readline();

    if ($input === 'quit') {
        echo 'Better luck next time' . PHP_EOL;
        break;
    }

    $input = (int)$input;

    if ($guessNumber > $input) {
        echo 'Too low...' . PHP_EOL;
    } else if ($guessNumber < $input) {
        echo 'Too high...' . PHP_EOL;
    } else {
        echo 'Thats it' . PHP_EOL;
        break;
    }
}