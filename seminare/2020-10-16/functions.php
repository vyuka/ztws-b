<?php

function writeln(string $mask, ...$params)
{
    echo sprintf($mask, ...$params) . PHP_EOL;
}

function add(int $a, int $b): int
{
    return $a + $b;
}

writeln('%s - %s', 'Hello', 'World!');

$a = [4, 5, 6];

$b = [1, 2, 3, ...$a, 7, 8, 9];

var_dump($b);

// writeln('Hello', 'World', '!');
// writeln('World');

// echo sprintf(
//     '%s %s %s %s',
//     1,
//     2,
//     3,
//     4
// );

$func = function ($a, $b) {
    echo 'Hello I am closure';
};

// $func(1, 2);

$names = ['A', 'B', 'C'];

// $toLowerCase = function ($input) {
//     return strtolower($input);
// };

// var_dump(array_map(function ($input) {
//     return strtolower($input);
// }, $names));

// Arrow functions

$arrowFunction = fn($a, $b) => $a + $b;

echo $arrowFunction(1, 2);


$myArr = [1, 2, 3, 4, 5, 6, 7, 8];

$doubled = array_map(fn (int $x): int => $x * 2, $myArr);

var_dump($doubled);