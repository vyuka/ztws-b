<?php
    function dd($var)
    {
        var_dump($var);
        exit;
    }

//    $uri = trim($_SERVER['REQUEST_URI'], '/');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <link
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"
    >

    <style>
        /* Sticky footer styles
        -------------------------------------------------- */
        html {
            position: relative;
            min-height: 100%;
        }

        body {
            /* Margin bottom by footer height */
            margin-bottom: 60px;
        }

        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            /* Set the fixed height of the footer here */
            height: 60px;
            line-height: 60px; /* Vertically center the text there */
            background-color: #f5f5f5;
        }


        /* Custom page CSS
        -------------------------------------------------- */
        /* Not required for template or sticky footer method. */

        body > .container {
            padding: 60px 15px 0;
        }

        .footer > .container {
            padding-right: 15px;
            padding-left: 15px;
        }

        code {
            font-size: 80%;
        }
    </style>

    <title>Moje stránka - O mně</title>
</head>

<body>

<?php include 'header.php'; ?>

<!-- Begin page content -->
<main role="main" class="container">
    <h1 class="mt-5">O mně</h1>

    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam autem deleniti dolorem est facere
    facilis fuga fugiat harum, nihil odit quis quisquam quod repudiandae rerum sit sunt vero vitae.
</main>

<?php include 'footer.php'; ?>

</body>
<html>