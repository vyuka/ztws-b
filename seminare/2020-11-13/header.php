<?php

//Ternary operator / Ternární operátor

$num = 42;

if ($num > 42) {
    $result = 'Yes';
} else {
    $result = 'No';
}

$result = $num > 42 ? 'Yes' : 'No';

function getNav()
{
    $uri = trim($_SERVER['REQUEST_URI'], '/');
    $result = '<ul class="navbar-nav mr-auto">' . PHP_EOL;

//    $navItems = ['', 'o-mne.php', 'kontakt.php'];

    $navItems = [
        'Domů' => ['index.php', ''],
        'O mně' => ['o-mne.php'],
        'Kontakt' => ['kontakt.php'],
//        'Kontakt 1' => ['kontakt.php'],
//        'Kontakt 2' => ['kontakt.php'],
//        'Kontakt 3' => ['kontakt.php'],
//        'Kontakt 4' => ['kontakt.php'],
    ];

    foreach ($navItems as $key => $item) {
        $li = sprintf(
            '<li class="nav-item %s"><a href="%s" class="nav-link">%s</a></li>' . PHP_EOL,
            in_array($uri, $item) ? 'active' : '',
//            $uri === $item ? 'active' : '', // Je $uri v poli $item?
            $item[0],
            $key
        );

        $result .= $li;
    }

    $result .= PHP_EOL . '</ul>';

    return $result;
}

?>
<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Fixed navbar</a>

        <?php echo 42 ?>
        <?= 42 ?>

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <?= getNav() ?>
        </div>
    </nav>
</header>